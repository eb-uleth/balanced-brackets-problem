/*
* CPSC 3620 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "brackets.h"
#include <stack>

using namespace std;

bool balancedBrackets(string input)
{
  // Stack to store the brackets in
  stack<char> brackets;
  for(string::iterator c = input.begin(); c != input.end(); ++c)
  {
    // If the character is not a bracket, ignore
    if (isBracket(*c))
    {
      // If the stack is empty, then push the bracket on
      if (brackets.empty())
      {
        brackets.push(*c);
      } else 
      {
        // If it's not empty, check if the top of the stack is the matching 
        // bracket. If so, pop the stack. If not, push the bracket on the stack
        if (brackets.top() == matchingBracket(*c)) {
          brackets.pop();
        } else {
          brackets.push(*c);
        }
      }
    }
  }

  // The stack is empty if and only if the stack is empty
  return brackets.empty();
}

bool isBracket(char c)
{
  return c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}';
}

char matchingBracket(char b)
{
  // For a given character, return the matching bracket. If the character is no-
  // a bracket, return the null character.
  if (b == '(') return ')';
  else if (b == ')') return '(';
  else if (b == '[') return ']';
  else if (b == ']') return '[';
  else if (b == '{') return '}';
  else if (b == '}') return '{';
  else return '\0';
}