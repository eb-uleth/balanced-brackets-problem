CXX=g++
CXXFLAGS= -std=c++0x

BRACKETS = ./brackets
$(BRACKETS):
	$(CXX) $(CXXFLAGS) -o $(BRACKETS) -I ./ \
	./*.cc

.DEFAULT_GOAL:= all
all: $(BRACKETS)
	$(BRACKETS)

.PHONY: clean
clean:
	rm -rf *~ ./*.o $(BRACKETS)