#### Balanced Brackets Problem

**Description**:  Determine if the brackets within a string are balanced

**Author****:  Everett Blakley

**Date**:  February 10th, 2020

##### Compilation
To compile program, run the `make` command in the program directory. The program will run automatically after compilation.

To remove executables from project directory, run `make clean`

##### Program usage
When prompted, enter in a string and the program will determine if the brackets are balanced. Special characters (i.e. `\n`, `\t`) will be ignored. Entering an empty string will terminate the program.

##### Examples

```
Please enter a string (enter empty string to quit): (x+y)
    The brackets are balanced
Please enter a string (enter empty string to quit): (x+y]
    The brackets are not balanced
Please enter a string (enter empty string to quit): These are no the brackets you are looking for
    The brackets are balanced
Please enter a string (enter empty string to quit):
Thanks for playing
```

