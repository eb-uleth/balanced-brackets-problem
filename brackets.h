/*
* CPSC 3620 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef BRACKETS_H
#define BRACKETS_H

#include <iostream>
#include <string>

bool balancedBrackets(std::string input);

bool isBracket(char c);

char matchingBracket(char b);

#endif // BRACKETS_H