  /*
  * CPSC 3620 Spring 2020
  * Copyright 2019 Everett Blakley
  * Everett Blakley <everett.blakley@uleth.ca>
  */

  #include "brackets.h"

  using namespace std;

  int main()
  {
    string input;

    bool quit = false;
    do {
      cout << "Please enter a string (enter empty string to quit): ";
      getline(cin, input);
      
      if (input.length() == 0)
      {
        cout << "Thanks for playing!" << endl;
        quit = true;
        break;
      }
      bool matching = balancedBrackets(input);
      matching 
        ? cout << "\tThe brackets are balanced"
        : cout << "\tThe brackets are not balanced";
      cout << endl;
    } while(!quit);

    return 0;
  }